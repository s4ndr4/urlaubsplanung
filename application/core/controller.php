<?php
/*
 * Class BaseController
 */

abstract class BaseController {
    
    public $db = null;
    public $template;
    protected $templateParams = [];

    public function render()
    {
        $this->addTemplateParam('baseUrl', URL);
        $this->addTemplateParam('homeUrl', URL . 'index.php');
        $this->addTemplateParam('cssUrl', URL_CSS_FOLDER);
        $this->addTemplateParam('imgUrl', URL_IMG_FOLDER);
        $this->addTemplateParam('jsUrl', URL_JS_FOLDER);
        $this->addTemplateParam('controller', strtolower(get_class($this)));

        session_start();
        if (count($_SESSION) > 0) {
            $this->addTemplateParam('isAdmin', $_SESSION['isAdmin']);
            $this->addTemplateParam('user', $_SESSION['user']);
        }
        //$this->addTemplateParam('controller', );

        $loader = new \Twig\Loader\FilesystemLoader(APP . 'views/page');
        $twig = new \Twig\Environment($loader, [
            'debug' => true,
        ]);
        $twig->addExtension(new \Twig\Extension\DebugExtension());
        $function1 = new \Twig\TwigFunction('checkEventDay', function ($eventArray,$day) {
            foreach($eventArray as $event) {
                if ($event['date_start'] === $day) {
                    return true;
                }
            }
        });

        $function2 =  new \Twig\TwigFunction('checkDaysBetweenEventDay',function ($eventArray,$day) {
            foreach($eventArray as $event) {
                if ($event['date_start'] <= $day && $event['date_end'] >= $day) {
                    return true;
                }
            }
        });
        $twig->addFunction($function1);
        $twig->addFunction($function2);

        echo $twig->render($this->template, $this->templateParams);
    }

    public function addTemplateParam($key, $param)
    {
        $this->templateParams[$key] = $param;
    }

    /*
     * open Database-Connection with PDO
     */
    public function conntectToDatabase()
    {
        //$options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS);
    }

     /*
     * Loads the "model".
     * @return object model
     */
    public function loadModel($modelName)
    {
        if (file_exists(APP . 'model/' . $modelName . '.php') === true) {
            require APP . 'model/' . $modelName . '.php';
            // create new "model" (and pass the database connection)
            return new $modelName($this->db);
        }
        return NULL;
    }
}