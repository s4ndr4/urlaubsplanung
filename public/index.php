<?php

// TODO replace the following code with namespaces

// constant that holds the ROOT-path
define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
// constant that holds the APPLICATION-path
define('APP', ROOT . 'application' . DIRECTORY_SEPARATOR);

// load Composer-dependencies
if (file_exists(ROOT . 'vendor/autoload.php')) {
    require_once ROOT . 'vendor/autoload.php';
}

// load application config
require_once APP . 'config/config.php';

// loadf helper 
require_once APP . 'libs/helper.php';

// load application class
require_once APP . 'core/application.php';
require_once APP . 'core/controller.php';
require_once APP . 'core/model.php';

// start the application
$app = new Application();
