<?php

class AdminOverview extends BaseController
{

    public $template = "adminView.html.twig";

    public $user;

    public $event;

    public function __construct()
    {
        if ($_SESSION['isAdmin'] === false) {
            header("Location:" . URL . "login/?error=true");
        }
        if ($this->event === null) {
            $this->event = $this->loadModel('Event');
        }
        if ($this->user === null) {
            $this->user = $this->loadModel('User');
        }

        $this->addTemplateParam('events', $this->getRequests());

    }

    public function getRequests()
    {
        $this->conntectToDatabase();
        $requests = $this->event->getRequests();

        foreach ($requests as $request) {

            $user          = $this->user->getUserById($request['user_id']);
            $requestNew[]  = array(
                'id'         => $request['id'],
                'type'       => $request['type'],
                'first_name' => $user['first_name'],
                'last_name'  => $user['last_name'],
                'date_start' => $request['date_start'],
                'date_end'   => $request['date_end'],
                'status'     => $request['status'],
            );
        }
        return $requestNew;
    }

    public function editRequest()
    {
        $values    = $_POST['edit'];
        $requestId = $values['requestId'];
        $status    = $values['status'];

        if ($status == '2') {
            $this->event->handleHolidayDays($requestId);
        }

        $this->event->editEvent($requestId, $status);
        $this->addTemplateParam('events', $this->getRequests());
    }

}