<?php

/*
* Configuration for: Error reporting
*/
define('ENVIRONMENT', 'development');

error_reporting(E_ALL);
ini_set("display_errors", 0);

if (ENVIRONMENT == 'development' || ENVIRONMENT == 'dev') {

}

/*
* Configuration for: URLs
*/
define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', 'https://');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);


define('URL_CSS_FOLDER', URL .URL_PUBLIC_FOLDER . '/css/');
define('URL_JS_FOLDER', URL .URL_PUBLIC_FOLDER . '/javascript/');
define('URL_IMG_FOLDER', URL .URL_PUBLIC_FOLDER . '/images/');

/*
* Configuration for: Database
*/
define('DB_TYPE', 'mysql');
define('DB_HOST', '127.0.0.1');
define('DB_NAME', 'urlaubsplanung');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_CHARSET', 'utf8');