<?php

/*
 * Class Register
 */
class Register extends BaseController {

    public $template = "register.html.twig";
    /**
     * @var string
     */
    private $username;
    /**
     * @var mixed
     */
    private $password;
    /**
     * @var mixed
     */
    private $firstName;
    /**
     * @var mixed
     */
    private $lastName;
    /**
     * @var mixed
     */
    private $birthday;
    /**
     * @var User
     */
    private $user;


    public function __construct()
    {
        $this->user = $this->loadModel('User');
    }

    public function newUser()
    {
        if (isset($_POST['register'])) {
            $this->username = $_POST['Username'];
            $this->password = $_POST['Password'];
            $this->firstName = $_POST['first_name'];
            $this->lastName = $_POST['last_name'];
            $this->birthday = $_POST['birthday'];

            if ($this->user->register($this->username,password_hash($this->password,PASSWORD_DEFAULT),$this->firstName,$this->lastName,$this->birthday)) {
                ob_start();
                session_start();
                $_SESSION['user']    = $this->username;
                $_SESSION['user_id'] = $this->user->getIdByUsername($this->username)["id"];
                $_SESSION['isAdmin'] = false;

                header('Location:'.URL.'login');exit();
            }

        }
    }
}