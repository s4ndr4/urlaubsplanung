<?php

require_once APP . 'libs/CalendarGenerator.php';

class Request extends BaseController
{

    public $template = "request.html.twig";

    public function __construct()
    {

    }

    public function addRequest() {
        $this->loadModel('Event')->addRequest();
    }

}