<?php

class User extends BaseModel
{
    public $table = "user";
    public $db;

    const DEFAULT_VAC_DAYS = 28;
    const USER_ROLE_ID = 3;


    public function __construct($db)
    {
        $this->db = $db;

    }

    /**
     * Get all songs from database
     */
    public function getAllUsers()
    {
        $sql = "SELECT * FROM user";
        return $this->execute($sql);
    }

    public function getUserById($id)
    {
        $sql = 'select * from user where id = ?';
        return $this->getOne($sql, array($id));
    }

    public function selectByUsername($username)
    {
        $sql = "SELECT * FROM user WHERE username = ?";
        return $this->getOne($sql, array($username));
    }

    public function getIdByUsername($username)
    {
        $sql = "SELECT id FROM user WHERE username = ?";
        return $this->getOne($sql, array($username));
    }

    public function getUserRole($type)
    {
        $sql = 'select value from role where type = ? and id = ?';
        return $this->getOne($sql, array($type, $_SESSION['user']['role_id']));
    }

    public function getUserHolidaysLeft() {
        $sql = "SELECT vac_days_left FROM user WHERE id = ?";
        return $this->getOne($sql, array($_SESSION['user_id']));
    }

    public function register($username,$password,$firstname,$lastname,$birthday)
    {
        $sql = "INSERT INTO user (username, first_name, last_name, birthday, date_joined, role_id, password, vac_days,vac_days_left) VALUES(?,?,?,?,?,?,?,?,?)";
        $params = array($username,$firstname,$lastname,$birthday,date('Y-m-d'),self::USER_ROLE_ID,$password,self::DEFAULT_VAC_DAYS,28);


        return $this->insert($sql,$params);
    }

}
