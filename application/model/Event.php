<?php


class Event extends BaseModel
{
    public $table = "event";
    public $db;

    const STATUS_ACCEPTED      = 1;
    const STATUS_ACCEPTED_TEXT = 'Genehmigt';
    const STATUS_DECLINED      = 2;
    const STATUS_DECLINED_TEXT = 'Abgelehnt';
    const STATUS_PENDING       = 3;
    const STATUS_PENDING_TEXT  = 'Ausstehend';

    const TYPE_HOLIDAYS      = 1;
    const TYPE_HOLIDAYS_TEXT = 'Urlaub';


    public function __construct($db)
    {
        $this->db = $db;
    }

    /**
     * Get all songs from database
     */

    public function addRequest()
    {

        $request = $_REQUEST['form'];
        $sql    = 'insert into event (type, user_id, date_start, date_end, status) values (?,?,?,?,?)';
        $params = array(
            self::TYPE_HOLIDAYS,
            $_SESSION['user_id'],
            $request['date_start'],
            $request['date_end'],
            self::STATUS_PENDING
        );

        if ($this->insert($sql, $params) === true ) {
            $days = $this->calculateDays($request);
            $this->withdrawDays($days);
        }
        header("Location:" . URL);
    }

    public function withdrawDays($days) {
        $vacDays  = $this->getUserDays() - $days;
        $this->editVacDaysLeft($vacDays);
    }

    public function getUserDays() {
        $sql      = 'select vac_days_left from user where id = ?';
        return $this->getOne($sql, array($_SESSION['user_id']))['vac_days_left'];
    }

    public function getUserDaysById($id) {
        $sql      = 'select vac_days_left from user where id = ?';
        return $this->getOne($sql, array($id))['vac_days_left'];
    }

    public function calculateDays($request)
    {
        $from = date_create($request['date_end']);
        $to   = date_create($request['date_start']);
        $diff = date_diff($to, $from);
        return $diff->days + 1;
    }

    public function editVacDaysLeft($vacDays, $userId = false) {
        $update = 'update user set vac_days_left = ? where id = ?';
        if ($userId !== false) {
            $this->insert($update, array($vacDays, $userId));
            return true;
        }
        $this->insert($update, array($vacDays, $_SESSION['user_id']));
    }

    public function getRequests()
    {
        $sql = 'select * from event';
        return $this->select($sql);
    }

    public function editEvent($requestId, $status)
    {
        $sql = 'update event set status = ? where id = ?';
        return $this->insert($sql, array($status, $requestId));
    }

    public function getEventsOfUser()
    {
        $sql = "SELECT date_start,date_end FROM event WHERE user_id = ? AND status = 1";
        return $this->select($sql,array($_SESSION['user_id']));
    }

    public function getUserHolidays()
    {
        $sql = 'select * from event where user_id = ?';
        $result = $this->select($sql, array($_SESSION['user_id']));

        $eventDetail = array();
        foreach ($result as $event) {
            $eventDetail[] = array(
                'type'       => $this->getEventType($event['type']),
                'date_start' => $event['date_start'],
                'date_end'   => $event['date_end'],
                'status'     => $this->getStatus($event['status']),
            );
        }
        return $eventDetail;
    }

    public function getEventType($type)
    {
        $type = (int)$type;
        if ($type === 1) {
            return self::TYPE_HOLIDAYS_TEXT;
        }
        return false;
    }

    public function getStatus($status)
    {
        $status = (int)$status;
        if ($status === self::STATUS_ACCEPTED) {
            return self::STATUS_ACCEPTED_TEXT;
        }
        if ($status === self::STATUS_DECLINED) {
            return self::STATUS_DECLINED_TEXT;
        }
        if ($status === self::STATUS_PENDING) {
            return self::STATUS_PENDING_TEXT;
        }
        return false;
    }

    function handleHolidayDays($requestId)
    {
        $event = $this->getEventById($requestId);
        $dates = array(
            'date_start' => $event['date_start'],
            'date_end'   => $event['date_end'],
        );
        $result = $this->calculateDays($dates) + $this->getUserDaysById($event['user_id']);
        $this->editVacDaysLeft($result, $event['user_id']);
    }

    function getEventById($requestId)
    {
        $sql = 'select * from event where id = ?';
        return $this->select($sql, array($requestId))[0];
    }

}
