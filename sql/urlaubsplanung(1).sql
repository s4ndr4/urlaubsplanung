-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 25. Mai 2021 um 18:11
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `urlaubsplanung`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `event`
--

CREATE TABLE `event` (
  `id` int(32) NOT NULL,
  `type` int(32) NOT NULL,
  `user_id` int(32) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date NOT NULL,
  `status` int(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `event`
--

INSERT INTO `event` (`id`, `type`, `user_id`, `date_start`, `date_end`, `status`) VALUES
(1, 1, 1, '2021-05-24', '2021-05-28', 2),
(3, 1, 2, '2021-06-01', '2021-06-04', 1),
(15, 1, 2, '2021-05-10', '2021-05-14', 2),
(46, 1, 2, '2021-05-17', '2021-05-21', 2),
(47, 1, 2, '2021-05-17', '2021-05-21', 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `role`
--

CREATE TABLE `role` (
  `id` int(32) NOT NULL,
  `type` char(32) DEFAULT NULL,
  `value` char(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `role`
--

INSERT INTO `role` (`id`, `type`, `value`) VALUES
(3, 'rights', 'user'),
(4, 'rights', 'admin');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `special_days`
--

CREATE TABLE `special_days` (
  `id` int(20) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `special_days`
--

INSERT INTO `special_days` (`id`, `date`, `name`) VALUES
(1, '2021-05-24', 'komischer Feiertag Tag');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `id` int(32) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `birthday` date NOT NULL,
  `date_joined` date NOT NULL,
  `role_id` int(32) NOT NULL DEFAULT 0,
  `username` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT 'password',
  `vac_days` int(32) NOT NULL,
  `vac_days_left` int(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`id`, `active`, `first_name`, `last_name`, `birthday`, `date_joined`, `role_id`, `username`, `password`, `vac_days`, `vac_days_left`) VALUES
(1, 1, 'Alexander', 'Fischer', '1999-12-29', '2019-08-01', 3, 'benutzer', '$2y$10$vvSzdlsUAC/eRFSCMIkpVuYGw.ct/QGhCmlGHDEFioCQpN4dl6.6.', 30, 30),
(2, 1, 'Kilian', 'Mezger', '2001-08-08', '2019-08-01', 4, 'admin', '$2y$10$vvSzdlsUAC/eRFSCMIkpVuYGw.ct/QGhCmlGHDEFioCQpN4dl6.6.', 30, 30);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indizes für die Tabelle `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `special_days`
--
ALTER TABLE `special_days`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `event`
--
ALTER TABLE `event`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT für Tabelle `role`
--
ALTER TABLE `role`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `special_days`
--
ALTER TABLE `special_days`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints der Tabelle `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
