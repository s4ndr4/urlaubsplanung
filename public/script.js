$(document).on('click', '.popup-container .close', function () {
    handlePopUp();
    $(this).parent('.popup-container').remove();
});

$(document).on('click', '.requests button', function () {
    handlePopUp();
    var requestId = $(this).attr('data-id');
    $('#popups').append('<div class="popup-container"> <div class="headline relative"> <h1 class="align-center">Antrag bearbeiten</h1> </div> <h1 class="close">x</h1> <div class="content"> <form name="request" method="post" action="AdminOverview/editRequest"> <button class="button" data-status="1" data-id="'+ requestId +'">Annehmen</button> <button class="button" data-status="2" data-id="'+ requestId +'">Ablehnen</button></form> </div> </div>');
});

$(document).on('click', 'form[name="request"] button', function () {

    var $element, status, requestId;

    $element  = $(this);
    requestId = $element.attr('data-id');
    status    = $element.attr('data-status');

    var edit = {
        'requestId' : requestId,
        'status' : status
    };

    $.ajax({
        method: "POST",
        url: "AdminOverview/editRequest",
        data: {
            edit : edit,
        }
    }).done(function( response ) {
        var html = $(response).find("table.requests");
        $('table.requests').replaceWith(html);
        $('.popup-container').remove();
        handlePopUp()
    });

    return false;
});

function handlePopUp() {
    var $element = $('#popups');
    if ($element.hasClass('shown') === false) {
        $element.addClass('shown');
        return true;
    }
    $element.removeClass('shown');
}

