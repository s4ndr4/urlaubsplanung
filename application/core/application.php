<?php

class Application
{
    /*
    * @var null The controller
    */
    private $url_controller = null;

    /*
    * @var null The method
     */
    private $url_action = null;

    /*
    * @var array URL parameters
    */
    private $url_params = array();

    /*
     * Application "start"
     * analyse url and call requested controller and method
     */
    public function __construct()
    {

        // set requested controller, method and parameter
        $this->splitUrl();

        //todo load user object instead
        $userLoggedIn = $this->checkForUser();

        // user must be logged in
        if ($this->url_controller !== 'login' && $userLoggedIn !== true) {
            header("Location:" . URL . "login");
            exit;
        }

        // check for controller ? load controller : load default callback 
        if (!$this->url_controller) {

            require_once APP . 'controller/Home.php';
            $page = new Home();
            $page->render();

        } elseif (file_exists(APP . 'controller/' . $this->url_controller . '.php') === true) {
            // if controller exists, then initiate controller
            require_once APP . 'controller/' . $this->url_controller . '.php';
            $this->url_controller = new $this->url_controller();

            if (method_exists($this->url_controller, $this->url_action) === true) {
                // if method exists, then execute method with array of arguments
                $this->url_controller->{$this->url_action}($this->url_params);
            }
            $this->url_controller->render();

        } else {
            // if controller is requested but not found, return exception page
            header('location: ErrorPage');
        }
    }

    /*
     * Get and split the URL
     */
    private function splitUrl()
    {
        if (isset($_GET['url'])) {

            // split URL
            $url = trim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            // Put URL parts into according properties
            $this->url_controller = isset($url[0]) ? $url[0] : null;
            $this->url_action = isset($url[1]) ? $url[1] : null;

            // Remove controller and action from the split URL
            unset($url[0], $url[1]);

            // Rebase array keys and store the URL params
            $this->url_params = array_values($url);
        }
    }

    private function checkForUser()
    {
        if ($this->url_controller === 'register') {
            return true;
        }
        session_start();

        if (count($_SESSION) > 0) {

            $userSession = $_SESSION['user_id'];

            if (isset($userSession) !== true) {
                return false;
            }

            $pdo = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS);
            $sql = "SELECT id FROM user WHERE id = '". $userSession . "'";
            $query = $pdo->prepare($sql);
            $query->execute();
            $entry = $query->fetch()[0];

            if (isset($entry) !== true) {
                return false;
            }

            return true;

        }

        return false;

    }
}