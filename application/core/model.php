<?php
/**
 * Class BaseModel
 */

abstract class BaseModel {
    
    public $table;

    protected $db;

    public function __construct()
    {
    }

    public function execute($sql)
    {
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll(); 
    }

    public function fetchOne($sql) {
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchOne();
    }

    public function insert($sql, $params)
    {

        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS);
        return $this->db->prepare($sql)->execute($params);
    }

    public function select($sql, $params = false)
    {

        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS);

        $stmt = $this->db->prepare($sql);
        if ($params != false) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOne($sql, $params = false)
    {
        $this->db = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS);

        $stmt = $this->db->prepare($sql);
        if ($params != false) {
            $stmt->execute($params);
        } else {
            $stmt->execute();
        }
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

}