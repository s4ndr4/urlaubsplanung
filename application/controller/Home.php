<?php

require_once APP . 'libs/CalendarGenerator.php';

/*
 * Class Home
 */

class Home extends BaseController {

    public $template = "calendar.html.twig";

    protected $user;
    private $event;

    public function __construct()
    {
        if (!isset($_GET['month']) && !isset($_GET['year'])) {
            $calendar = new CalendarGenerator();
        } else{
            $calendar = new CalendarGenerator($_GET['year'],$_GET['month'],$_GET['day']);
        }
        if ($this->user === null) {
            $this->user = $this->loadModel('User');
        }

        if ($this->event === null) {
            $this->event = $this->loadModel('Event');
        }

        $this->addTemplateParam('days',$calendar->renderDays());
        $this->addTemplateParam('month',$calendar->renderMonth());
        $this->addTemplateParam('current',$calendar->renderCurrent());
        $this->addTemplateParam('prev',$calendar->renderPrev());
        $this->addTemplateParam('next',$calendar->renderNext());
        $this->addTemplateParam('event', $calendar->renderEvent($this->event));


        $holidaysLeft = $this->user->getUserHolidaysLeft();
        $holidays = $this->event->getUserHolidays();
        $this->addTemplateParam('holidays', $holidays);
        $this->addTemplateParam('holidaysLeft', $holidaysLeft['vac_days_left']);
    }

}