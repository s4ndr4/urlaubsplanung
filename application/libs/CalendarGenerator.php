<?php


class CalendarGenerator
{
    private $date;
    private $year;
    private $month;
    private $day;

    public function __construct($year = '', $month = '', $day = '')
    {
        $this->year = $year;
        $this->month = $month;
        $this->day = $day;

        $this->date = new DateTime();
        if ($year && $month && $day) {
            $this->date->setTimestamp(strtotime($this->year . '-' . $this->month . '-' . $this->day));

        }

    }

    public function renderDays()
    {
        $days = [];
        $lastDayOfMonth = $this->date->format('t');

        for($i = 1; $i <= $lastDayOfMonth; $i++) {
            $day = $this->date->format('Y-m-'). '' .$i;
            $days[] = array("day" => $day);
        }

        return $days;
    }

    public function renderMonth()
    {
        return $this->date->format('M');
    }

    public function renderCurrent()
    {
        return $this->date->format('D M d o');
    }

    public function renderPrev()
    {
        $this->date = $this->date->sub(date_interval_create_from_date_string('1 month'));

        return '?month=' . $this->date->format('m') . '&year=' . $this->date->format('Y') . '&day=' . $this->date->format('d') ;
    }

    public function renderNext()
    {
        $this->date = $this->date->add(date_interval_create_from_date_string('2 months'));

        return '?month=' . $this->date->format('m') . '&year=' . $this->date->format('Y') . '&day=' . $this->date->format('d') ;
    }

    public function renderEvent($event)
    {
        return $event->getEventsOfUser();
    }
}