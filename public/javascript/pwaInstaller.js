async function registerSw() {
    if ('serviceWorker' in navigator) {
        try {
            await navigator.serviceWorker.register('./serviceworker.js');
            console.log('SW registration succeded');
        } catch (e) {
            console.log('SW registration failed');
        }
    }
}

registerSw();