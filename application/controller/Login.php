<?php

/*
 * Class Home
 */
class Login extends BaseController {

    public $template = "login.html.twig";

    public function __construct()
    {
        if (isset($_GET) === true) {
            if ($_GET['login'] == 'failed') {
                $this->addTemplateParam('MESSAGE_TYPE', "ERROR");
                $this->addTemplateParam('MESSAGE', 'You have entered a wrong username or password!');
            }
        }

        // img api
        $img = $this->callAPI();

        $this->addTemplateParam('RANDOM_IMG', $img);
    }

    function callAPI()
    {
        return 'https://source.unsplash.com/random';
    }

    public function validate()
    {
        if(count($_POST) > 0) {

            $userName       = $_POST['user_name'];
            $userPassword   = $_POST['password'];

            $this->conntectToDatabase();
            $userModel = $this->loadModel('User');
            $user = $userModel->selectByUsername($userName);

            // username found in db
            if (isset($user) === true) {

                $correctLogin = password_verify($userPassword, $user['password']);

                if ($correctLogin === true) {

                    session_start();
                    $_SESSION['user']    = $user;
                    $_SESSION['user_id'] = $user['id'];
                    $_SESSION['isAdmin'] = false;
                    if ($userModel->getUserRole('rights')['value'] === 'admin') {
                        $_SESSION['isAdmin'] = true;
                    }

                    $this->success();
                    return;
                }
            }
        }
        $this->failed();
    }

    public function success()
    {
        header("Location:" . URL . "index.php");
        exit;
    }

    public function failed()
    {
        header("Location:" . URL . "login&login=failed");
        exit;
    }

    public function logout()
    {
        session_destroy();
        header("Location:" . URL . "login");
        exit;
    }

}